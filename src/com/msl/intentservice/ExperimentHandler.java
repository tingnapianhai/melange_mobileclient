package com.msl.intentservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.msl.melange.Config;
import com.msl.melange.NetworkCommunication;

public class ExperimentHandler extends WakefulIntentService1 {
	String TAG = "ExperimentHandler";
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	JSONObject work;
	JSONObject trigger;
	String workload;
	String trigtype;
	JSONArray arr;
	int trigvalue;
	int exp_count;
	int sampling_rate;
	boolean record_location;
	private static PendingIntent pending_intent;
	private static AlarmManager mgr = null;

	public static boolean cancelMgr() {
		if (mgr != null) {
			mgr.cancel(pending_intent);
			mgr = null;
			return true;
		} else
			return false;
	}

	public ExperimentHandler() {
		super("ExperimentHandler");
	}

	@Override
	protected void doWakefulWork(Intent intent) {
		Config con = new Config(this);
		String wkld = NetworkCommunication.gettrigworkload(con.getExpId()+"");
		
		try {
			work = new JSONObject(wkld);
			Log.d(TAG, "JSON file: " + wkld);
			
			//work = work.getJSONObject("experiment").getJSONObject("workload");
			arr = work.getJSONObject("experiment").getJSONObject("workload").getJSONArray("tasks");
			
//			arr = work.getJSONObject("experiment").getJSONObject("workload").getJSONArray("tasks");//tasks
//			settings = getSharedPreferences(Config.Status_file, 0);//private mode
//			editor = settings.edit();
//    	    editor.putInt(Config.TASK_TOTAL_COUNT, arr.length());// total count of tasks
//    	    editor.commit();
 //   	    Log.d(TAG, "There are " + Integer.toString(arr.length()) + " tasks to execute");  =  	    
    	    sampling_rate = work.getJSONObject("experiment").getInt("sampling_rate");
    	    record_location = work.getJSONObject("experiment").getBoolean("record_location");
    	    
//    		Intent newIntent = new Intent(context, WorkloadHandler.class);
//			newIntent.putExtra("workload", wkld);
//			newIntent.putExtra("sampling_rate", sampling_rate);
//			newIntent.putExtra("record_location", record_location);
//			newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			context.startActivity(newIntent);
    	    
    	    if(arr.getJSONObject(0).getString("category").equals("Download")|| (arr.getJSONObject(0).getString("category").equals("Video") && arr.getJSONObject(0).getString("mode").equals("Download"))){
				Log.d(TAG, "OnCreate: Category is Download");
				Intent i = new Intent(this, DownloadTask.class);
				i.putExtra("sampling_rate", work.getJSONObject("experiment").getInt("sampling_rate"));// workload string
				i.putExtra("record_location", work.getJSONObject("experiment").getBoolean("record_location"));
				i.putExtra("url", arr.getJSONObject(0).getString("url"));
				i.putExtra("taskid", arr.getJSONObject(0).getInt("id"));
				i.putExtra("number", 0);// the current number of task
				i.putExtra("workload", work.toString());
				WakefulIntentService1.sendWakefulWork(this, i);
			}
    	    
			else if(arr.getJSONObject(0).getString("category").equals("Watchdog")){
				Log.d(TAG, "OnCreate: category is WatchDog");
//				com.msl.old.WakefulIntentService.acquireStaticLock(this,work.getJSONObject("experiment").getString("id"));// id is exp id
				Intent i = new Intent(this, DownloadDog.class);				
				i.putExtra("sampling_rate", work.getJSONObject("experiment").getInt("sampling_rate"));// workload string
				i.putExtra("record_location", work.getJSONObject("experiment").getBoolean("record_location"));
				i.putExtra("duration", arr.getJSONObject(0).getInt("duration"));//duration
				i.putExtra("taskid", arr.getJSONObject(0).getInt("id"));
				i.putExtra("number", 0);// the current number of task
				i.putExtra("workload", work.toString());
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				this.startActivity(i);
			}
    	    
//			if (work.getJSONObject("experiment").has("trigger")) {
//				//The experiment has a trigger
//				
//				Log.d(TAG, "Experiment with trigger");
//				trigger = work.getJSONObject("experiment").getJSONObject("trigger");
//				trigtype = trigger.getString("category");
//				trigvalue = Integer.parseInt(trigger.getString("value"));
//				exp_count = work.getJSONObject("experiment").getInt("running_time");
//				String Until_time = work.getJSONObject("experiment").getString("until");
//				exp_count = exp_count / trigvalue; 
//				
//				workload = work.getJSONObject("experiment").getJSONObject("workload").toString();
//				
//				editor.putInt(Config.EXP_TOTAL_COUNT, exp_count); // How many times the workload has to run
//				editor.putInt(Config.EXP_CUR_COUNT, 0); // How many times the workload has run
//				editor.commit();
//				
////				serviceIntent = new Intent(context, TriggerAlarmReceiver.class);
////				serviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
////
////				serviceIntent.putExtra("workload", wkld);
////				serviceIntent.putExtra("sampling_rate", sampling_rate);
////				serviceIntent.putExtra("record_location", record_location);
////				// serviceIntent.putExtra("frequency", value);
////				pending_intent = PendingIntent.getBroadcast(context, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
////				mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
////				mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 1000, trigvalue*1000, pending_intent);
//				
//			}
//			
//			else {
//				// The experiment doesn't contain a trigger
//
//				Log.d(TAG, "Experiment without trigger");
//							
//				Intent newIntent = new Intent(context, WorkloadHandler.class);
//				newIntent.putExtra("workload", wkld);
//				newIntent.putExtra("sampling_rate", sampling_rate);
//				newIntent.putExtra("record_location", record_location);
//				newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				context.startActivity(newIntent);
//								
//			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}
