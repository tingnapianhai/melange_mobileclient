package com.msl.intentservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

abstract public class WakefulIntentService extends IntentService {
	static Context con;
	abstract void doWakefulWork(Intent intent,String message,Context con);
	public static final String LOCK_NAME_STATIC = "com.msl.IntentService";
	private static PowerManager.WakeLock lockStatic = null;
	private static String message;

	/**
	 * 
	 @param context Context object to acquire the lock
	 @param msg passes on to the worker thread
	 
	 @return Acquires a static lock
	 * 
	 */
	public static void acquireStaticLock(Context context, String msg) {
		Log.v("com.msl.melange","ack lock "+msg);
		//getLock(context).acquire();
		message = msg;
		con = context;
	}
	
	public static void setMsg(String msg) {
		message = msg;
	}

	synchronized private static PowerManager.WakeLock getLock(Context context) {
		if (lockStatic == null) {
			PowerManager mgr = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			lockStatic = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOCK_NAME_STATIC);
			lockStatic.setReferenceCounted(true);
		}
		return (lockStatic);
	}

	public WakefulIntentService(String name) {
		super(name);
	}

	@Override
	final protected void onHandleIntent(Intent intent) {  // onHandleIntent is a protected method of class "IntentService"
		try {
			doWakefulWork(intent, message, con);
		} finally {
			System.out.println("Lock is being released now ");
//			getLock(this).release();
		}
	}
}
