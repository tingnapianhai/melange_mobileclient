package com.msl.intentservice;

import com.msl.receiver.BatteryReceiver;
import com.msl.receiver.LocationMonitor;
import com.msl.receiver.LocationMonitorNetwork;
import com.msl.receiver.SignalListener;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class ResultRecordService extends Service {
	
	LocationMonitor locationlistener;
	LocationManager locationManager;
	LocationMonitorNetwork locationlistenerNetwork;
	LocationManager locationManagerNetwork;
	TelephonyManager telephonyInfo;// ******************
	SignalListener signallistener;
	BatteryReceiver bat_stat;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		signallistener = new SignalListener(this);
		telephonyInfo = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS );
		
		locationlistener = new LocationMonitor(this);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationlistenerNetwork = new LocationMonitorNetwork(this);
		locationManagerNetwork = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationlistener);
		locationManagerNetwork.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationlistenerNetwork);
		
		bat_stat = new BatteryReceiver();
		registerReceiver(bat_stat, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		
	}

	@Override
	public void onDestroy() {
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_NONE );
		locationManager.removeUpdates(locationlistener);
		locationManager.removeUpdates(locationlistenerNetwork);
		unregisterReceiver(bat_stat);
		super.onDestroy();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//return super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}
	

}
