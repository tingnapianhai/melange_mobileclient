package com.msl.intentservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.msl.melange.Config;
import com.msl.melange.NetworkCommunication;
import com.msl.old.WakefulIntentService;
import com.msl.receiver.Statistics;

public class PongService extends WakefulIntentService1{
	SharedPreferences settings;

	public PongService() {
		super("PongService");
	}
	
	protected void doWakefulWork(Intent intent) {
		
		settings = getSharedPreferences(Config.Status_file, 0);//private mode
		
		NetworkCommunication.sendstatus(Statistics.pongResp(intent, this), new Config(this).getTermId(), this);
	}
}
