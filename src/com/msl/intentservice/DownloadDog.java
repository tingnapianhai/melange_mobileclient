//********************
// 2011-12-01 realize the watchdog function 
//********************

package com.msl.intentservice;

import java.util.Timer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;

import com.msl.melange.R;
import com.msl.old.RecordTask;
import com.msl.old.ServerUploadTask;
import com.msl.receiver.BatteryReceiver;
import com.msl.receiver.LocationMonitor;
import com.msl.receiver.LocationMonitorNetwork;
import com.msl.receiver.SignalListener;

public class DownloadDog extends Activity {

    static final int SERVER_UPLOAD_FREQUENCY = 15*1000;
    static final int Positioning_Accuracy_Check = 300*1000; // checking frequency
	private static final String TAG = "DownloadDog Task";
	private Bundle extras;
	int duration;
	int task_id;
	int sample_rate;
	boolean record_location;
	String urlfile = null;
	int trigtime = 0;
	
	Button button_finish;
	Button button_1;
	Button button_2;
	Button button_start;
	Timer timerRecord = new Timer();
	Timer timerServerUpload = new Timer();
	Timer timerPosition = new Timer();
	//Timer timerdownload = new Timer();
	
	//register a LocationMonitor
	LocationMonitor locationlistener;
	LocationManager locationManager;
	LocationMonitorNetwork locationlistenerNetwork;
	LocationManager locationManagerNetwork;
	BatteryReceiver bat_stat;
	
	TelephonyManager telephonyInfo;
	SignalListener signallistener;

	@Override
	public void onCreate(Bundle bundle) {
		Log.d(TAG, "Starting WatchdogTask");
		super.onCreate(bundle);
		setContentView(R.layout.watchdog);
		
		signallistener = new SignalListener(getApplicationContext());
		telephonyInfo = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);// ******************
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS |PhoneStateListener.LISTEN_CELL_LOCATION );
		
		locationlistener = new LocationMonitor(this);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationlistenerNetwork = new LocationMonitorNetwork(this);
		locationManagerNetwork = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
//		locationManager.addGpsStatusListener(gpslistener);
		

		bat_stat = new BatteryReceiver();
		registerReceiver(bat_stat, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationlistener);
		locationManagerNetwork.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationlistenerNetwork);

		extras = getIntent().getExtras();
		duration = extras.getInt("duration");
		sample_rate = extras.getInt("sampling_rate");   // sample rate when running task 		
		record_location = extras.getBoolean("record_location");
		task_id = extras.getInt("taskid");

		setTitle("Start Watchdog");
		Thread WatchdogThread = new Thread(null, RunnableWatchdog, "watchdog_thread");
		WatchdogThread.start();
	} 
	
	private Runnable RunnableWatchdog = new Runnable() {
		public void run() {
			backgroundWatchdog();
		}
	};
	
	private void backgroundWatchdog(){
		
		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE); 
		//WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP, "MyWakeLock"); // for Samsung
		WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock"); //for others, Google HTC //屏幕自动会关闭
		wakeLock.acquire((duration+1)*1000);
		//wakeLock.acquire();
			
		//Timer timerRecord = new Timer();
		RecordTask rectask = new RecordTask(getIntent(), this, task_id);
		// every sample_rate seconds, get result of statistics
		timerRecord.scheduleAtFixedRate(rectask, 0, sample_rate*1000);
		
		//Timer timerServerUpload = new Timer();
		ServerUploadTask uploadtask = new ServerUploadTask(rectask, this);
		timerServerUpload.scheduleAtFixedRate(uploadtask, SERVER_UPLOAD_FREQUENCY, SERVER_UPLOAD_FREQUENCY);	
		SystemClock.sleep(duration*1000);
		
		// to make sure that the last records are uploaded to the server
		uploadtask.run();
		
		timerRecord.cancel();
		timerServerUpload.cancel();
		timerPosition.cancel();
		timerRecord = null;
		timerServerUpload = null;
		timerPosition = null;
		
		//wakeLock.release();
		finish();
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		locationManager.removeUpdates(locationlistener);
		locationManagerNetwork.removeUpdates(locationlistenerNetwork);
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_NONE );
		unregisterReceiver(bat_stat);
		//finish();
	}
	
//    GpsStatus.Listener gpslistener = new GpsStatus.Listener() {
//        public void onGpsStatusChanged(int event) {
//            switch (event) {
//            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
//                //Log.i(TAG, "卫星状态改变");
//                GpsStatus gpsStatus=locationManager.getGpsStatus(null);
//                //maxSatellites = gpsStatus.getMaxSatellites();
//                int count = 0;
//                int countall = 0;
//                int gpsfixtime = 0;
//                gpsfixtime = gpsStatus.getTimeToFirstFix();
//                gpsfixtime = gpsfixtime/1000;
//                
//                Iterator<GpsSatellite> iters = gpsStatus.getSatellites().iterator();
//
//                while ( iters.hasNext() )
//                {
//                	countall++;
//                	GpsSatellite s = iters.next();
//                    if(s.usedInFix())
//                    	count++;
//                   }
//                
//                editor = settings.edit();
//                editor.putInt(Config.SatellitesUsing, count);
//                editor.putInt(Config.SatellitesVisible, countall);
//                editor.putInt(Config.GPSTimetoFirstFix, gpsfixtime);
//        		editor.commit();
//                
//                break;
//            case GpsStatus.GPS_EVENT_STARTED:
//                Log.i(TAG, "定位启动");
//                editor = settings.edit();
//                editor.putInt(Config.SatellitesUsing, 0);
//                editor.putInt(Config.SatellitesVisible, 0);
//        		editor.commit();
//                break;
//            case GpsStatus.GPS_EVENT_STOPPED:
//                Log.i(TAG, "定位结束");
//                editor = settings.edit();
//                editor.putInt(Config.SatellitesUsing, 0);
//                editor.putInt(Config.SatellitesVisible, 0);
//        		editor.commit();
//                break;
//            case GpsStatus.GPS_EVENT_FIRST_FIX:
//                Log.i(TAG, "第一次定位");
//                break;
//            }
//        };
//    };

}

