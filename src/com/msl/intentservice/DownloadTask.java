package com.msl.intentservice;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import com.msl.melange.Config;
import com.msl.old.RecordTask;

public class DownloadTask extends WakefulIntentService1{
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	static File temp;
	private Timer timer;
	RecordTask rectask;
	String trigtype;
	Float trigvalue;
	int sample_rate;
	boolean record_location;
	JSONObject work;
	JSONObject trigger;
	JSONArray arr;
	String path;
	String TAG = "DownloadTask";

	public DownloadTask() {
		super("DownloadService");
	}
	@Override
	protected void doWakefulWork(Intent intent) {
		settings = getSharedPreferences(Config.Status_file, 0);//private mode
		
		Log.d(TAG,"Download started");		
		Bundle b = intent.getExtras();
		sample_rate = b.getInt("sampling_rate");
		record_location = b.getBoolean("record_location");
		timer = new Timer();
		rectask = new RecordTask(intent, this, b.getInt("taskid"));
		this.startService(new Intent(this, ResultRecordService.class));
		timer.scheduleAtFixedRate(rectask, 0, sample_rate*1000);
		
		try {
			String url = b.getString("url");
			Log.d(TAG,"Downloading url: " + url);
			String file = Config.getData(url);
			
			File fi = new File(file);
			if(fi.exists())fi.delete();
		} catch (Exception e) {
			e.printStackTrace();
			setAlarm(this, b);
		}
		
		rectask.cancel();
		timer.cancel();
		timer = null;
		String res = rectask.getResults();
		
//		com.msl.intentservice.ResultUploadService.acquireStaticLock(getApplicationContext(),res);// id is exp id
		Intent upload = new Intent(this, ResultUploadService.class);
		upload.putExtra("results",res);//results
//		upload.putExtra("sampling_rate", sample_rate);
//		upload.putExtra("record_location", record_location);
//		startService(upload); // result uploader worker thread
		
		WakefulIntentService1.sendWakefulWork(this, upload);
		this.stopService(new Intent(this,ResultRecordService.class));
		
//		Log.d(TAG, "Calling NetworkCommunication to upload data to server");
//		settings = getSharedPreferences(Config.Status_file, 0);//private mode
//		NetworkCommunication.sendResult(res, settings.getInt(Config.EXP_ID_LATEST, -1), this);
		
		
		
		setAlarm(this, b);
		
	}
	
	public void setAlarm(Context context, Bundle b){
		try {
			
			
			work = new JSONObject(b.getString("workload"));
			
			SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date d1 = df.parse(work.getJSONObject("experiment").getString("until"));
			
			if (work.getJSONObject("experiment").has("trigger") && new Date().before(d1)){
				Log.v("com.msl.melange",work.getJSONObject("experiment").getJSONObject("trigger").getInt("value")+"");
				Intent i = new Intent(context, DownloadTask.class);
				i.putExtra("sampling_rate", sample_rate);// workload string
				i.putExtra("record_location", record_location);
				i.putExtra("url", b.getString("url"));
				i.putExtra("taskid",  b.getInt("taskid"));
				i.putExtra("workload", work.toString());
				
				PendingIntent pi = PendingIntent.getService(context, 0, i,PendingIntent.FLAG_ONE_SHOT );
				
				AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,SystemClock.elapsedRealtime()+(work.getJSONObject("experiment").getJSONObject("trigger").getInt("value")*1000) , pi);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
