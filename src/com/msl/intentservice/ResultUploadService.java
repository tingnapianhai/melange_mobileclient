package com.msl.intentservice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.msl.melange.Config;
import com.msl.melange.NetworkCommunication;

public class ResultUploadService extends WakefulIntentService1{
	String TAG = "ResultUploadService";
	SharedPreferences settings;

	public ResultUploadService() {
		super("ResultUploadService");
	}
	
	protected void doWakefulWork(Intent intent) {
		String message = intent.getExtras().getString("results");
		Log.d(TAG, "Calling NetworkCommunication to upload data to server");
		NetworkCommunication.sendResult(message, new Config(this).getExpId(), this);
		
	}
}
