//********************
// 2011-12-01 realize the watchdog function 
//********************

package com.msl.old;

import java.util.Timer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.msl.melange.Config;
import com.msl.melange.R;
import com.msl.receiver.LocationMonitor;
import com.msl.receiver.LocationMonitorNetwork;
import com.msl.receiver.SignalListener;

public class WatchdogTask extends Activity {

    static final int SERVER_UPLOAD_FREQUENCY = 15*1000; 
	private static final String TAG = "WatchdogTask";
	private Bundle extras;
	int duration;
	int task_id;
	int sample_rate;
	boolean record_location;
	
	Button button_finish;
	Timer timerRecord = new Timer();
	Timer timerServerUpload = new Timer();
	
	//register a LocationMonitor
	LocationMonitor locationlistener;
	LocationManager locationManager;
	LocationMonitorNetwork locationlistenerNetwork;
	LocationManager locationManagerNetwork;
	TelephonyManager telephonyInfo;// ******************
	SignalListener signallistener;
	
	SharedPreferences settings;
	SharedPreferences.Editor editor;

	@Override
	public void onCreate(Bundle bundle) {
		Log.d(TAG, "Starting WatchdogTask");
		super.onCreate(bundle);
		setContentView(R.layout.watchdog);
		
		signallistener = new SignalListener(this);
		telephonyInfo = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);// ******************
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS |PhoneStateListener.LISTEN_CELL_LOCATION );
		
		locationlistener = new LocationMonitor(this);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationlistenerNetwork = new LocationMonitorNetwork(this);
		locationManagerNetwork = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationlistener);
		locationManagerNetwork.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationlistenerNetwork);
		
		//button_finish = (Button)findViewById(R.id.button_finish);
		button_finish.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				timerRecord.cancel();
				timerServerUpload.cancel();
				WatchdogTask.this.finish();	
			}
		});
		
		extras = getIntent().getExtras();
		duration = extras.getInt("duration");
		sample_rate = extras.getInt("sampling_rate");   // sample rate when running task 		
		record_location = extras.getBoolean("record_location");
		task_id = extras.getInt("taskid");

		settings = getSharedPreferences(Config.Status_file, 0); //private mode
		Thread WatchdogThread = new Thread(null, RunnableWatchdog, "watchdog_thread");
		WatchdogThread.start();

	} 
	
	private Runnable RunnableWatchdog = new Runnable() {
		public void run() {
			backgroundWatchdog();
			//finish();
		}
	};
	
	private void backgroundWatchdog(){
		
		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE); 
//		WakeLock wakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP|PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakeLock"); 
		WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP, "MyWakeLock"); // for Samsung
		//WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock"); //for others, Google HTC
		wakeLock.acquire((duration+1)*1000);
		//wakeLock.acquire();
			
		//Timer timerRecord = new Timer();
		RecordTask rectask = new RecordTask(getIntent(), this, task_id);
		// every sample_rate seconds, get result of statistics
		timerRecord.scheduleAtFixedRate(rectask, 0, sample_rate*1000);
		
		//Timer timerServerUpload = new Timer();
		ServerUploadTask uploadtask = new ServerUploadTask(rectask, this);
		timerServerUpload.scheduleAtFixedRate(uploadtask, SERVER_UPLOAD_FREQUENCY, SERVER_UPLOAD_FREQUENCY);
		
		SystemClock.sleep(duration*1000);
		
		// to make sure that the last records are uploaded to the server
		uploadtask.run();
		
		timerRecord.cancel();
		timerServerUpload.cancel();
		
		//wakeLock.release();
		finish();
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		locationManager.removeUpdates(locationlistener);
		//locationManager.removeUpdates(locationlistenerNetwork);
		telephonyInfo.listen(signallistener, PhoneStateListener.LISTEN_NONE );
	}

}


