package com.msl.old;

import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.msl.receiver.AppMonitor;
import com.msl.receiver.Statistics;

//This class is used to record the statistics (such as battery and signal level) while performing some task
public class RecordTask extends TimerTask {
	private static final String TAG = "RecordTask";
	Intent intent;
	Context context;
	JSONArray results;
	int taskid;
	String res;
	boolean record_location;

	public RecordTask(Intent intent, Context context, int taskid) {
		this.intent = intent;
		this.context = context;
		results = new JSONArray();
		this.taskid = taskid;
		this.record_location = this.intent.getExtras().getBoolean("record_location");
	}
	
	public void run() {
		JSONObject sample = null;
		//Log.d(TAG, "Record task to static");
		
		//Uid of application - melange
		PackageManager pm = context.getPackageManager();
		int app_uid = 0;
		try {
			ApplicationInfo appInfo = pm.getApplicationInfo(context.getPackageName(), 0);
			if(appInfo!=null)
				app_uid = appInfo.uid;
			else app_uid = 0;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}// make it run only once and not every time.
		//Uid of application - melange
//**********************
		AppMonitor.getTransApp (context);
		int number = AppMonitor.appnum;
		long systemRx = AppMonitor.systemdataRx;
		long totalRx = AppMonitor.systemrecord;
		if(number>0) {
		for(int i=0;i<number;i++) {
		sample = Statistics.getSample(intent, context, taskid, app_uid, systemRx,totalRx,this.record_location);
		try { 
			//Log.d("mn", i+"***" + number + "**********"  + AppMonitor.app_name.size());
			sample.put("active_pid", AppMonitor.app_name.get(i));
			sample.put("active_tx", AppMonitor.app_Txbyte.get(i));
			sample.put("active_rx", AppMonitor.app_Rxbyte.get(i));
		} 
		//try {sample.put("bssid", AppMonitor.app_Txbyte.get(i)+"");} 
		catch (JSONException e) { e.printStackTrace(); }
		
		synchronized (results) {
			results.put(sample);
		}
		}//for
		}//if
		
		if(number == 0) {
			sample = Statistics.getSample(intent, context, taskid, app_uid,systemRx,totalRx,this.record_location);
			try { 
				//Log.d("mn", "***" + number + "**********"  + AppMonitor.app_name.size());
				sample.put("active_pid", "N-O");
				sample.put("active_tx", 0);
				sample.put("active_rx", 0);
				} 
			catch (JSONException e) { e.printStackTrace(); }
			
			synchronized (results) {
				results.put(sample);
			}
		}
		
//************************
	}

	public String getResults() {
		JSONObject response = new JSONObject();
		try {
			synchronized (results) {
				response.put("results", results);
				if(results.length() == 0) {
					return "{}";
				}
				results = new JSONArray();
			}
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

}
