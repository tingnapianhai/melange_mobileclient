package com.msl.old;

import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.msl.intentservice.ResultUploadService;
import com.msl.intentservice.WakefulIntentService1;
import com.msl.receiver.NetworkMonitor;

public class ServerUploadTask extends TimerTask {
	String TAG = "ServerUploadTask";
	RecordTask recorder;
	Context context;

	public ServerUploadTask(RecordTask recorder,  Context context) {
		this.recorder = recorder;
		this.context = context;
	}
	
	public void run() {
		Log.d(TAG, "Starting service ResultUploadService to upload data to server");
		if(NetworkMonitor.getNetworkStatus(context)) { // make sure when the network is unavailable, the statistics data is not missing
		String results = recorder.getResults();
		if(results != "{}") {
			Intent upload = new Intent(context, ResultUploadService.class);
			upload.putExtra("results",results);//results			
			WakefulIntentService1.sendWakefulWork(context, upload);
		}
		}//if network status
	}

}
