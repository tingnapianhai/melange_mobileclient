//********************
// modified by K, 2011-12-01 realize the watchdog function 
//********************
/*
 * if duration = 0, then ignore duration, it means the statistic continue until the audio playing finish
 * if duration > 0, it means the statistic continue until the activity runs out duration seconds
 * */
// Summary: 2011-12-01 make it in the onPrepared(), then no excel file.
//                     but if make it in the onCompletion(), then has excel file.

//called by "WorkloadHandler"

package com.msl.old;

import java.io.IOException;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.msl.intentservice.ResultUploadService;
import com.msl.melange.Config;
import com.msl.melange.R;

public class MediaPlayerTask extends Activity implements
		OnBufferingUpdateListener, OnCompletionListener, OnPreparedListener,
		OnVideoSizeChangedListener, OnErrorListener, OnInfoListener, SurfaceHolder.Callback {

	private static final String TAG = "MediaPlayerTask";
	private int mVideoWidth;
	private int mVideoHeight;
	private MediaPlayer mMediaPlayer;
	private SurfaceView mPreview;
	private SurfaceHolder holder;
	private Bundle extras;
	public static final int STREAM_AUDIO = 1;
	public static final int DOWNLOAD_AUDIO = 2;
	public static final int STREAM_VIDEO = 5;
	public static final int DOWNLOAD_VIDEO = 6;
	private int media_mode;
	private boolean mIsVideoSizeKnown = false;
	private boolean mIsVideoReadyToBePlayed = false;
	private int duration;
	private int task_id;
	String url;
	RecordTask rectask;
	private Timer timer;
	int sample_rate;
	boolean record_location;
	JSONObject work;
	JSONArray arr;
	String path;
	
	SharedPreferences settings;
	SharedPreferences.Editor editor;

	public void onCreate(Bundle icicle)   
	{
		Log.d(TAG, "Starting MediaPlayerTask");
		super.onCreate(icicle);
		setContentView(R.layout.mediaplayer);
		extras = getIntent().getExtras();
		
		media_mode = extras.getInt("media_mode");
		url = extras.getString(("value"));
		
		duration = extras.getInt("duration");
		sample_rate = extras.getInt("sampling_rate");   
		record_location = extras.getBoolean("record_location");
		
		task_id = extras.getInt("taskid");

		settings = getSharedPreferences(Config.Status_file, 0);//private mode

		try {
			work = new JSONObject(getIntent().getExtras().getString(("workload")));
			arr = work.getJSONObject("experiment").getJSONObject("workload").getJSONArray("tasks");//tasks
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (media_mode == STREAM_VIDEO || media_mode == DOWNLOAD_VIDEO) {
			mPreview = (SurfaceView) findViewById(R.id.surface);
			holder = mPreview.getHolder();
			holder.addCallback(this);
			holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		
		else if (media_mode == STREAM_AUDIO || media_mode == DOWNLOAD_AUDIO) {
			playAudio(media_mode, url);
		}
		
		else {
			Log.d(TAG, "Cannot understand the media mode to play. Media_mode: " + media_mode);
			setResult(Activity.RESULT_CANCELED, null);  
			finish();
		}
		
	} 
	
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated called with url "+ url);
		playVideo(media_mode, url);
	}

	public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {
		Log.d(TAG, "surfaceChanged called");
	}

	public void surfaceDestroyed(SurfaceHolder surfaceholder) {
		Log.d(TAG, "surfaceDestroyed called");
	}

	private void startStatisticsLogger(){
		timer = new Timer();
		rectask = new RecordTask(getIntent(), this, task_id);
		timer.scheduleAtFixedRate(rectask, 0, sample_rate*1000); 
	}

	private String stopStatisticsLogger(){
		rectask.cancel();
		timer.cancel();
		timer = null;
		return rectask.getResults();		
	}

	private void playAudio(Integer Media, String path) {
		doCleanUp();
		try {
			switch (media_mode) {			
			case STREAM_AUDIO:
				Log.d(TAG, "Streaming Audio: " + path);
				break;
				
			case DOWNLOAD_AUDIO:
				path = downloadMedia();
				Log.d(TAG, "Downloaded Audio to play: "  + path);
				break;
			} 

			startStatisticsLogger();    
			 
			// Create a new media player and set the listeners
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setDataSource(path);
			
			mMediaPlayer.setScreenOnWhilePlaying(false);
			mMediaPlayer.setWakeMode(getBaseContext(), PowerManager.PARTIAL_WAKE_LOCK);
			mMediaPlayer.setOnBufferingUpdateListener(this);
			
			mMediaPlayer.setOnPreparedListener(this);   //gen xia mian dian dao shun xu le
			mMediaPlayer.setOnCompletionListener(this); //gen shang mian dian dao shun xu le
			
			mMediaPlayer.setOnInfoListener(this);
			mMediaPlayer.setOnErrorListener(this);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mMediaPlayer.prepareAsync(); 
//			mMediaPlayer.start();

		} catch (Exception e) {
			Log.e(TAG, "error: " + e.getMessage(), e);
		}
		
	} 

	
	private void playVideo(Integer Media, String path) {
		doCleanUp();
		try {
			switch (Media) {
			
			case STREAM_VIDEO:
				Log.d(TAG, "Streaming Video: " + path);
				break;
				
			case DOWNLOAD_VIDEO:
				path = downloadMedia();
				Log.d(TAG, "Downloading Video to play: "  + path);
				break;
			}

			startStatisticsLogger();
			
			// Create a new media player and set the listeners
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setDataSource(path);
			mMediaPlayer.setDisplay(holder);
			mMediaPlayer.setOnBufferingUpdateListener(this);
			mMediaPlayer.setOnCompletionListener(this);
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.setOnVideoSizeChangedListener(this);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mMediaPlayer.prepare();

		} catch (Exception e) {
			Log.e(TAG, "error: " + e.getMessage(), e);
		}

		
	}  

	
	public void onBufferingUpdate(MediaPlayer arg0, int percent) {
		Log.d(TAG, "onBufferingUpdate percent:" + percent);
	}

	public void onCompletion(MediaPlayer arg0) {
		Log.d(TAG, "onCompletion called");
		
		if(duration == 0)   
		{
		String res = stopStatisticsLogger();
		
		WakefulIntentService.acquireStaticLock(getApplicationContext(), res);
		Intent i = new Intent(this, ResultUploadService.class);
		startService(i); 

		setResult(Activity.RESULT_OK, null); 
		finish();
		}
	} 
	
	public void onPrepared(MediaPlayer mediaplayer) {
		Log.d(TAG, "onPrepared called");
		
		if (media_mode == STREAM_VIDEO || media_mode == DOWNLOAD_VIDEO) {
			mIsVideoReadyToBePlayed = true;
			if (mIsVideoReadyToBePlayed && mIsVideoSizeKnown) {
				startVideoPlayback();
			}
		}		
		else if (media_mode == STREAM_AUDIO || media_mode == DOWNLOAD_AUDIO) {
			mMediaPlayer.start();
		}
		
		
		if(duration > 0)  
		{
			SystemClock.sleep(duration*1000);     
			String res = stopStatisticsLogger();	  
			WakefulIntentService.acquireStaticLock(getApplicationContext(), res); 
			Intent i = new Intent(this, ResultUploadService.class); 
			startService(i); 
			setResult(Activity.RESULT_OK, null); 
			finish(); 
		}
		
	}
	
	private void startVideoPlayback() {
		Log.v(TAG, "startVideoPlayback");
		holder.setFixedSize(mVideoWidth, mVideoHeight);
		mMediaPlayer.start();
	}

	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		Log.v(TAG, "onVideoSizeChanged called");
		if (width == 0 || height == 0) {
			Log.e(TAG, "invalid video width(" + width + ") or height(" + height + ")");
			return;
		}
		mIsVideoSizeKnown = true;
		mVideoWidth = width;
		mVideoHeight = height;
		if (mIsVideoReadyToBePlayed && mIsVideoSizeKnown) {
			startVideoPlayback();
		}
	}


	public boolean onError(MediaPlayer mp, int what, int extra) {
		Log.d(TAG, "onError called: " + what + " : " + extra);
		return false;
	}
	
	public boolean onInfo(MediaPlayer mp, int what, int extra){
		Log.d(TAG, "onInfo called: " + what + " : " + extra);
		return false;
	}
	
	@Override
	protected void onPause() {
		Log.d(TAG, "onPause called");
		super.onPause();

	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy called");
		super.onDestroy();
		releaseMediaPlayer();
		doCleanUp();
	}
	
	private void releaseMediaPlayer() {
		Log.d(TAG, "releaseMediaPlayer called");
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}
	
	private void doCleanUp() {
		Log.d(TAG, "doCleanUp called");
		mVideoWidth = 0;
		mVideoHeight = 0;
		mIsVideoReadyToBePlayed = false;
		mIsVideoSizeKnown = false;
	}
	
	public String downloadMedia() {
		String path = "";
		
		try {
			Log.d(TAG,"Download starting...");	
			int cur_task = settings.getInt(Config.TASK_CUR_COUNT, -1);

			Timer timer_download = new Timer();
			RecordTask rectask_download = new RecordTask(getIntent(), getBaseContext(), 0);
			// TODO: Maybe that getBaseContext should be the current context. change if fails ;)
			
			// that 0 is the current task_id, we set it to zero to distinguish between playing and downloading the video
			timer_download.scheduleAtFixedRate(rectask_download, 0, sample_rate*1000); 
			
			//Here the video is downloaded
			path = Config.getData(arr.getJSONObject(cur_task).getString("url"));
			 
			rectask_download.cancel();
			timer_download.cancel();
			timer_download = null;
			String res = rectask_download.getResults();
			
			WakefulIntentService.acquireStaticLock(getApplicationContext(), res);// id is exp id
			Intent i = new Intent(this, ResultUploadService.class);
			startService(i); // result uploader worker thread
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return path;	
	}
	

	
} 