package com.msl.old;

import java.io.File;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.util.Log;

import com.msl.melange.Config;

public class DownloadTimerTask extends TimerTask {
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	static File temp;
	RecordTask rectask;
	String trigtype;
	Float trigvalue;
	int sample_rate;
	boolean record_location;
	JSONObject work;
	JSONObject trigger;
	JSONArray arr;
	String path;
	String TAG = "DownloadTask";
	String url_file = null;
	
	public DownloadTimerTask(String url) {
		super();
		url_file = url;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			String url = url_file;
			Log.d(TAG,"Downloading url: " + url);
			String file = Config.getData(url);
			File fi = new File(file);
			fi.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
