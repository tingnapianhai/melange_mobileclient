package com.msl.receiver;

import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

import com.msl.melange.Config;

public class Statistics {
	static TelephonyManager tm ;
	static GsmCellLocation gsm = null;
	static WifiManager wifi; //
	static WifiInfo info; //
 //	static SharedPreferences settings;
	static Config config;
	
	static double latitude;
	static double longitude;
	
	
	public static JSONObject getSample(Intent intent, Context context, int taskid, int app_uid, long systemRx, long totalRx,boolean record_location){
		
		config = new Config(context);
		
		int cellid = CellMonitor.getCellid(context);
		int lac = CellMonitor.getLac(context);
		
		tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);//
		info = wifi.getConnectionInfo();//
		String wifi_BSSID = info.getBSSID();
		boolean wifi_status = true;
		if (wifi_BSSID == null) {wifi_BSSID = "NO WIFI"; wifi_status = false;}
		
		int current_value = 0;
		current_value = CurrentReaderFactory.getValue2();
		
		Date d = new java.util.Date();
		Timestamp ts = new java.sql.Timestamp(d.getTime());
		JSONObject child_obj = new JSONObject();
		try {
			child_obj.put("datetime", ts.toString());
			child_obj.put("task_id", taskid);
			child_obj.put("terminal_id", config.getTermId());
			child_obj.put("bits_sent", TrafficStats.getTotalTxBytes());
			child_obj.put("bits_receive", totalRx);
			if(app_uid != 0) {
				child_obj.put("bits_sent_app", TrafficStats.getUidTxBytes(app_uid));
				child_obj.put("bits_receive_app", systemRx);
			}
			else {
				child_obj.put("bits_sent_app", 0);
				child_obj.put("bits_receive_app", 0);
			}
			;
			child_obj.put("cellid", cellid );
			child_obj.put("lac", lac );

			child_obj.put("scr", ScreenMonitor.Screen_Status(context));		
			child_obj.put("net", NetworkMonitor.getNetworkStatus(context));
			child_obj.put("mcc", context.getResources().getConfiguration().mcc);
			child_obj.put("mnc",context.getResources().getConfiguration().mnc);
			
			child_obj.put("signal", config.getSignal());
			if(!record_location){
				child_obj.put("latitude", 0); 
				child_obj.put("longitude", 0);
				child_obj.put("accuracy", 0);
			}else{
				child_obj.put("latitude", getLatitude2(context));
				child_obj.put("longitude", getLongitude2(context));
				child_obj.put("accuracy", getAccuracy2(context));
			}
			child_obj.put("connection_type", tm.getNetworkType());
			child_obj.put("battery_level", config.getBatlvl());
			child_obj.put("battery_status", config.getBatPlugged());// plugged in or !//TODO
			child_obj.put("battery_voltage", current_value); // current
			child_obj.put("wifi_status", wifi_status);
			child_obj.put("bssid", wifi_BSSID);
			
			child_obj.put("gps", Locationcase.getGPS(context));
			child_obj.put("foreground_pid", AppMonitor.getForeApp(context));
			
			return child_obj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
//	private static boolean getWifiStatus (Intent intent) {
//		NetworkInfo info = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
//		if(info!=null && info.isConnected()==true) {
//			if(info.getTypeName().equals("WIFI"))
//				return true;
//			else return false;
//		}
//		return false;
//	}
	
	private static float getLatitude2(Context context) {
		if(config == null)config = new Config(context);
		float latitude =0;
		if(Locationcase.getGPS(context))
			latitude = config.getLatitude()==0 ?config.getLatitudeN() :config.getLatitude();
		else
			latitude = config.getLatitudeN();
			return latitude;
	}
	
	private static float getLongitude2(Context context) {
		float longitude =0;
		if (Locationcase.getGPS(context))
			longitude = config.getLongitude() == 0 ?config.getLongitudeN() :config.getLongitude();
		else 
			longitude = config.getLongitudeN();
			return longitude;
	}
	
	private static float getAccuracy2(Context context) {
		float accuracy =0;
		if (Locationcase.getGPS(context))
			accuracy = config.getAccuracy() == 0 ?config.getAccuracyN() :config.getAccuracy() ;
		else 
			accuracy =config.getAccuracyN();
			return accuracy;
	}

	public static String pongResp(Intent intent , Context context){
		
		if(config==null) config = new Config(context);

		Date d = new java.util.Date();
		Timestamp ts = new java.sql.Timestamp(d.getTime());
		JSONObject parent_obj = new JSONObject();
		JSONObject child_obj = new JSONObject();
		JSONArray json_arr = new JSONArray();
		try {
			child_obj.put("datetime",ts.toString());
			child_obj.put("cellid",gsm.getCid() & 0xffff);
			child_obj.put("lac",gsm.getLac());
			child_obj.put("mcc", context.getResources().getConfiguration().mcc);
			child_obj.put("mnc",context.getResources().getConfiguration().mnc);
			child_obj.put("signal",config.getSignal());
			child_obj.put("latitude",0);
			child_obj.put("longitude",0);
			child_obj.put("connection_type", tm.getNetworkType());
			child_obj.put("battery_level", 0); 
			child_obj.put("battery_status",0);// plugged in or !
			child_obj.put("battery_voltage", 0);
			//child_obj.put("battery_voltage", BatteryReceiver.getCurrentNow());
			child_obj.put("wifi_status",false);
			child_obj.put("bssid", 0);
			
			json_arr.put(child_obj);
			parent_obj.put("statuses", json_arr);

			return parent_obj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
