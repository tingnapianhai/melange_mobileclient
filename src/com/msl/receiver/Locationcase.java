package com.msl.receiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;

public class Locationcase {

	private static final String TAG = "Locationcase";
	static SharedPreferences settings;
	static double latitude;
	static double longitude;
	static float accuracy;
	static boolean GPS_sta = true;
	//
	public static boolean getGPS (Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		GPS_sta = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		return GPS_sta;
	}
	public static double getlatitude(Context context){
		latitude = 0;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		latitude = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude();
        return latitude;
	}
	
	public static double getlongitude(Context context){
		longitude = 0;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		longitude = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude();
        return longitude;
	}
	
	public static float getaccuracy(Context context){
		accuracy = 0;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		accuracy = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getAccuracy();
        return accuracy;
	}
	//
	
}
