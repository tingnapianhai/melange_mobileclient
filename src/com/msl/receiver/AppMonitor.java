package com.msl.receiver;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.net.TrafficStats;

public class AppMonitor {
	static Vector<Long> hashrx = change();
	static Vector<Long> hashtx = change();
	public static long systemdataRx = (long) 0; // data transferring by system
												// service
	public static long systemrecord = TrafficStats.getTotalRxBytes();
	public static int appnum = 0;
	public static String appname = "";
	public static int foreuid = 0;
	public static int activeuid = 0;
	public static ArrayList<String> app_name = new ArrayList<String>();
	public static ArrayList<Long> app_Txbyte = new ArrayList<Long>();
	public static ArrayList<Long> app_Rxbyte = new ArrayList<Long>();
	public static ArrayList<Integer> app_uid = new ArrayList<Integer>();

	public static String getForeApp(Context context) {
		// String TAG = "Foreground_App";
		appnum = 0;
		appname = "";
		String name = "Null or setting";
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();

		for (RunningAppProcessInfo appProcess : appProcesses) {
			// if(appProcess.importance ==
			// RunningAppProcessInfo.IMPORTANCE_FOREGROUND
			// && !isRunningService(appProcess.processName, context) )
			if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND
					&& appProcess.uid > 10000) {
				name = ScreenMonitor.Screen_Status(context) + "-"
						+ appProcess.processName;
				foreuid = appProcess.uid;
				break;
			}
		}
		return name;
	} // getForeApp

//	public static String getBackApp(Context context) {
//		String name = "Background: ";
//		ActivityManager activityManager = (ActivityManager) context
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningAppProcessInfo> appProcesses = activityManager
//				.getRunningAppProcesses();
//		for (RunningAppProcessInfo appProcess : appProcesses) {
//			if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
//				name = name + " + " + appProcess.processName + " "
//						+ TrafficStats.getUidRxBytes(appProcess.uid);
//			}
//		}
//		return name;
//	} // getBackApp

//	public static String getVisibleApp(Context context) {
//		String name = "Visible: ";
//		ActivityManager activityManager = (ActivityManager) context
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningAppProcessInfo> appProcesses = activityManager
//				.getRunningAppProcesses();
//		for (RunningAppProcessInfo appProcess : appProcesses) {
//			if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_VISIBLE) {
//				name = name + " + " + appProcess.processName;
//			}
//		}
//		return name;
//	} // getVisibleApp

	public static Vector<Long> change() {
		Vector<Long> hash2 = new Vector<Long>();
		for (int i = 0; i < 200; i++)
			hash2.add(i, (long) 0);
		return hash2;
	}

	public static void getTransApp(Context context) {
		systemdataRx = 0;

		appnum = 0;
		appname = "";
		app_name = new ArrayList<String>();
		app_Txbyte = new ArrayList<Long>();
		app_Rxbyte = new ArrayList<Long>();
		app_uid = new ArrayList<Integer>();
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();

		systemdataRx = TrafficStats.getTotalRxBytes() - systemrecord;
		systemrecord = TrafficStats.getTotalRxBytes();

		for (RunningAppProcessInfo appProcess : appProcesses) {
			int appuid = appProcess.uid;
			long Rxdata = TrafficStats.getUidRxBytes(appuid);
			long Txdata = TrafficStats.getUidTxBytes(appuid);
			long Rxcha = 0;
			long Txcha = 0;

			int hash_num = 0;
			if (appuid >= 10000)
				hash_num = appuid - 10000;
			if (appuid < 10000)
				hash_num = appuid - 1000;
			if (appuid >= 10200)
				hash_num = 199;// why??

			if (hash_num < 200 && (Rxdata > 0 || Txdata > 0))
			// if( Rxdata>0 || Txdata>0 )
			{
				// if(Rxdata>0 && hashmark.elementAt(hash_num)!= (long)1) {
				// if(Rxdata>0)
				// systemdataRx = systemdataRx + Rxdata;
				// hashmark.setElementAt((long) 1, hash_num); //做标记 如maps
				// }
				Long valueR = hashrx.elementAt(hash_num);
				Long valueT = hashtx.elementAt(hash_num);
				long value_Rx = valueR.longValue();
				long value_Tx = valueT.longValue();

				if (value_Rx != Rxdata) // &&& 2012-07-02
				{
					Rxcha = Rxdata - value_Rx;
					hashrx.setElementAt(Rxdata, hash_num); // &&& 2012-07-02

					if (Rxcha < 0)
						Rxcha = 0;
					systemdataRx = systemdataRx - Rxcha;
				}
				if (value_Tx != Txdata) {
					Txcha = Txdata - value_Tx;
					hashtx.setElementAt(Txdata, hash_num);
					if (Txcha < 0)
						Txcha = 0;
				}
				if (value_Tx != Txdata || value_Rx != Rxdata) {
					app_name.add(appnum, appProcess.processName);
					app_Rxbyte.add(appnum, Rxcha);
					app_Txbyte.add(appnum, Txcha);
					app_uid.add(appnum, appProcess.uid);
					appnum++; // 如果符合条件再加
				}
			}
		} // ends for
			// return app_name;
	} // getTransApp

//	public static void getTransAppTest(Context context) {
//		appnum = 0;
//		appname = "";
//		app_name = new ArrayList<String>();
//		app_Txbyte = new ArrayList<Long>();
//		app_Rxbyte = new ArrayList<Long>();
//		ActivityManager activityManager = (ActivityManager) context
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningAppProcessInfo> appProcesses = activityManager
//				.getRunningAppProcesses();
//
//		for (RunningAppProcessInfo appProcess : appProcesses) {
//			int appuid = appProcess.uid;
//			long Rxdata = TrafficStats.getUidRxBytes(appuid);
//			long Txdata = TrafficStats.getUidTxBytes(appuid);
//			long Rxcha = 0;
//			long Txcha = 0;
//
//			int hash_num = 0;
//			if (appuid >= 10000)
//				hash_num = appuid - 10000;
//			if (appuid < 10000)
//				hash_num = appuid - 1000;
//			if (appuid >= 10200)
//				hash_num = 199;
//
//			// if( appuid<10200 && (Rxdata>0 || Txdata>0) )
//			if (Rxdata > 0 || Txdata > 0) {
//				Long valueR = hashrx.elementAt(hash_num);
//				Long valueT = hashtx.elementAt(hash_num);
//				long value_Rx = valueR.longValue();
//				long value_Tx = valueT.longValue();
//
//				if (value_Rx < Rxdata) {
//					Rxcha = Rxdata - value_Rx;
//					hashrx.setElementAt(Rxdata, hash_num);
//				}
//				if (value_Tx < Txdata) {
//					Txcha = Txdata - value_Tx;
//					hashtx.setElementAt(Txdata, hash_num);
//				}
//				if (value_Tx < Txdata || value_Rx < Rxdata) {
//					app_name.add(appnum, appProcess.uid + " " + value_Rx + " "
//							+ Rxdata);
//					app_Rxbyte.add(appnum, Rxcha);
//					app_Txbyte.add(appnum, Txcha);
//					appnum++; // 如果符合条件再加
//				}
//			}
//		} // ends for
//			// return app_name;
//	} // getTransApp
//
//	public static String getBackTransApp(Context context) {
//		String name = "Background: ";
//		ActivityManager activityManager = (ActivityManager) context
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningAppProcessInfo> appProcesses = activityManager
//				.getRunningAppProcesses();
//		for (RunningAppProcessInfo appProcess : appProcesses) {
//			int appuid = appProcess.uid;
//			if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND
//					&& TrafficStats.getUidTxBytes(appuid) != 0
//					&& TrafficStats.getUidRxBytes(appuid) != 0) {
//				name = name + " + " + appProcess.processName;
//			}
//		}
//		return name;
//	} // getBackTransApp
//
//	private static boolean isRunningService(String processname, Context context) {
//
//		if (processname == null)
//			return false;
//		RunningServiceInfo service;
//		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningServiceInfo> l = activityManager.getRunningServices(9999);
//		Iterator<RunningServiceInfo> i = l.iterator();
//		while (i.hasNext()) {
//			service = i.next();
//			if (service.process.equals(processname))
//				return true;
//		}
//		return false;
//	} // isRunningService ends

}

class Databyte {
	public long app_Txbyte = 0;
	public long app_Rxbyte = 0;
}
