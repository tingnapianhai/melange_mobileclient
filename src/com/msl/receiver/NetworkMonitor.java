package com.msl.receiver;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkMonitor {
	private Context context;
	
	public NetworkMonitor(Context con){
		this.context = con;
	}
	//
	public static boolean getNetworkStatus (Context context) { 
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); 
		NetworkInfo info = connManager.getActiveNetworkInfo();
		if(info!=null && info.isConnected())
			return true;
		else 
			return false;
	}
}
