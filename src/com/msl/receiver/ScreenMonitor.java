package com.msl.receiver;

import android.content.Context;
import android.os.PowerManager;

public class ScreenMonitor {
	
	public static int Screen_Status (Context context) {
		PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		int status = 0;
		if(pm.isScreenOn())
			status = 1;
		return status;
	}
	
	public static String Screen_Status2 (Context context) {
		PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		String status = "ON";
		if(pm.isScreenOn())
			status = "ON";
		else
			status = "OFF";
		return status;
	}

}

