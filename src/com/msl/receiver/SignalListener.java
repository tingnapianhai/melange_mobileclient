package com.msl.receiver;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

import com.msl.melange.Config;

public class SignalListener extends PhoneStateListener {
	
	Context con;
	
	@Override
	public void onSignalStrengthsChanged(SignalStrength signalStrength) {
		super.onSignalStrengthsChanged(signalStrength);
		
		Config config = new Config(con);
		config.setSignal(signalStrength.getGsmSignalStrength());
		
//		Melange p = Melange.getRef();
//		settings = p.getSharedPreferences(Config.Status_file, 0);//private mode
//		editor = settings.edit();
//		editor.putLong(Config.SIG_STR, signalStrength.getGsmSignalStrength());
//		editor.putLong(Config.SIG_DBM, signalStrength.getCdmaDbm());
//		editor.putLong(Config.SIG_ECIO, signalStrength.getCdmaEcio());
//		editor.putLong(Config.SIG_STRDBM, signalStrength.getGsmSignalStrength()*2 - 113);
//		editor.commit();
		
		//Log.v("signallistener", "on signal change called");
	}
	
	public SignalListener(Context context) {
		con = context;
	}

	@Override
	public void onCellLocationChanged (CellLocation location) {
		super.onCellLocationChanged(location);
//		Melange p = Melange.getRef();
//		settings = p.getSharedPreferences(Config.Status_file, 0);//private mode
//		editor = settings.edit();
		
//		editor.putLong(Config.Cellid, gsm.getCid());
//		editor.putLong(Config.Lac, gsm.getLac());
//		editor.putInt(Config.Cellid, gsm.getCid() );
//		editor.putInt(Config.Lac, gsm.getLac() );
//		Log.v("signallistener", "on location change called");
		
//		editor.commit();
	}

}
