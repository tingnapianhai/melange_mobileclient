package com.msl.receiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.msl.melange.Config;

public class LocationMonitorNetwork implements LocationListener{

//	private static final String TAG = "Locationcase";
	static double latitude;
	static double longitude;	
	static boolean GPS_sta = true;
	static double gps_latitude = 0;
	static double gps_longitude = 0;
	private Context context;
	
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	
	public LocationMonitorNetwork(Context con){
		this.context = con;
	}
	//
	public static boolean getGPS (Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		GPS_sta = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		return GPS_sta;
	}
	public static double getlatitude(Context context){
		latitude = 0;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		if(getGPS(context))
			latitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
		else
			latitude = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude();
        return latitude;
	}
	
	public static double getlongitude(Context context){
		longitude = 0;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); // final
		if(getGPS(context))
			longitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
		else
			longitude = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude(); 
        return longitude;
	}
	
//	public void getLocation (Context context) {
//		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//		
////		// Network
////		Criteria crit = new Criteria();
////		crit.setPowerRequirement(Criteria.POWER_LOW);
////		crit.setAccuracy(Criteria.ACCURACY_COARSE);
////		String provider = locationManager.getBestProvider(crit, false);
//		
//		// GPS
//		Criteria crit2 = new Criteria();
//		crit2.setAccuracy(Criteria.ACCURACY_FINE);
//		String provider2 = locationManager.getBestProvider(crit2, false);
//		
//		locationManager.requestLocationUpdates(provider2, 1000, 1, this);
//		
//	}
	
	@Override
	public void onLocationChanged(Location arg0) {

		Config config = new Config(context);		
		config.setLongitudeN((float)arg0.getLongitude());
		config.setLatitudeN((float) arg0.getLatitude());
		config.setAccuracyN((float) arg0.getAccuracy());
	}
	@Override
	public void onProviderDisabled(String arg0) {
	
	}
	@Override
	public void onProviderEnabled(String arg0) {

	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}
	
}
