package com.msl.receiver;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public class CellMonitor {
	private Context context;
	static TelephonyManager tm ;
	static GsmCellLocation gsm = null;
	public CellMonitor(Context con){
		this.context = con;
	}
	//
	public static int getCellid (Context context) { 
		tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		gsm = (GsmCellLocation) tm.getCellLocation();
		int cellid = 0;
		if(gsm!=null) {
		cellid = gsm.getCid();
		}
		return cellid;
	}
	
	public static int getLac (Context context) { 
		tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		gsm = (GsmCellLocation) tm.getCellLocation();
		int lac = 0;
		if(gsm!=null) {
		lac = gsm.getLac();
		}
		return lac;
	}
	
}
