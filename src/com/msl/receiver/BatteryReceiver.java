package com.msl.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import com.msl.melange.Config;

public class BatteryReceiver extends BroadcastReceiver {

//	private static String TAG = "BatteryReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Config con = new Config(context);
		
		int bat_sts = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		int bat_lvl = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		
		con.setBatPlugged(bat_sts);
		con.setBatlvl(bat_lvl);


	}
	
}
