package com.msl.melange;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.c2dm.C2DMessaging;

public class Melange extends Activity {
	Button refresh;
	TextView account;
	Button unregister;
	TextView terminalid;
	public static String LOG = "Melange";

	private ProgressThread progThread;
	private ProgressDialog progDialog;
	Config con;

	private IntentFilter c2dmRegistrationStatusIntentFilter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		con = new Config(this);
		if (con.getMslRegStatus()) {// already registered
			setRegView();
		} else {
			c2dmRegistrationStatusIntentFilter = new IntentFilter(C2DMReceiver.C2DM_REGISTERED_STATUS_ACTION);
			registerReceiver(registrationStatusReceiver,c2dmRegistrationStatusIntentFilter);
			showDialog(0);
		}
	}
	
	
	
	private void setRegView(){
		setContentView(R.layout.main_reg);
		account = (TextView) findViewById(R.id.account);
        account.setText("MSL Terminal ID");       
        terminalid = (TextView) findViewById(R.id.terminalid);
        terminalid.setText(con.getTermId());	
	}

	public void onClick(View v) {
		switch(v.getId()) {
        	case R.id.refresh:
        		boolean bool = NetworkCommunication.refreshRegistrationId(con.getTermId(),C2DMessaging.getRegistrationId(v.getContext()));
				if(bool)Toast.makeText(v.getContext(), "Registration Refreshed!!", Toast.LENGTH_SHORT).show();
				else Toast.makeText(v.getContext(), "Registration Could not be Refreshed!!", Toast.LENGTH_SHORT).show();
        		break;
        	case R.id.unregister:	
        		C2DMessaging.unregister(getApplicationContext());
        		con.setMslRegStatus(false);
			    finish();
			    break;
		}
	}
	
	public boolean setRegStoreTermId(Context context){
		String terminal_id = NetworkCommunication.sendRegistrationId(context,C2DMessaging.getRegistrationId(context));
		if(terminal_id!=null){
			con.setMslRegStatus(true);
			con.setTermId(terminal_id);
			dismissDialog(0);
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					setRegView();
				}
			});
			return true;
		}
		else return false;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		progDialog = new ProgressDialog(this);
		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progDialog.setTitle("Melage Registration");
		progDialog.setMessage("Please Wait");
		progDialog.setCancelable(false);
		progThread = new ProgressThread();
		progThread.start();
		return progDialog;
	}

	private class ProgressThread extends Thread {
		boolean c2dmstate;

		@Override
		public void run() {
			C2DMessaging.register(getApplicationContext(), Config.C2DM_SENDER);
			try {
				synchronized (this) {
					wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				finish();
			}
			if (c2dmstate == true) {
				unregisterReceiver(registrationStatusReceiver);
				setRegStoreTermId(getApplicationContext());
			}else{
				finish();
			}
		}

		public void setgcmState(boolean state) {
			c2dmstate = state;
		}

	}

	private BroadcastReceiver registrationStatusReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			int registrationStatus = intent.getExtras().getInt(
					C2DMReceiver.INTENT_C2DM_REGISTRATION_STATUS);
			switch (registrationStatus) {
			case (C2DMReceiver.C2DM_REGISTRATION_SUCCESSFUL): {
				synchronized (progThread) {
					progThread.setgcmState(true);
					progThread.notify();
				}
				break;
			}
			case (C2DMReceiver.C2DM_REGISTRATION_ERROR): {
				String errorId = intent.getExtras().getString(
						C2DMReceiver.INTENT_C2DM_REGISTRATION_ERROR_ID);
				Log.v(LOG, "c2dmerror" + errorId);
				synchronized (progThread) {
					progThread.setgcmState(false);
					progThread.notify();
				}
				break;
			}
			case (C2DMReceiver.C2DM_UNREGISTERED): {
				Log.v(LOG, "C2DM service unregistered");
				synchronized (progThread) {
					progThread.setgcmState(false);
					progThread.notify();
				}
				break;
			}
			default: {
				Log.w(LOG, "C2DM registration status not recognized "
						+ registrationStatus);
				synchronized (progThread) {
					progThread.setgcmState(false);
					progThread.notify();
				}
			}
			}
		}

	};
}
