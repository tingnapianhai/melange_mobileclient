package com.msl.melange;

import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.c2dm.C2DMBaseReceiver;
import com.google.android.c2dm.C2DMessaging;
import com.msl.intentservice.ExperimentHandler;
import com.msl.intentservice.PongService;
import com.msl.intentservice.WakefulIntentService1;

public class C2DMReceiver extends C2DMBaseReceiver {
	static final String TAG = "C2DMReceiver";
	
	public static final String C2DM_REGISTERED_STATUS_ACTION = "c2dmregistered";
	public static final String INTENT_C2DM_REGISTRATION_STATUS = "c2dmregistrationstatus";
	public static final String INTENT_C2DM_REGISTRATION_ERROR_ID = "c2dmregistrationerrorid";
	public static final int C2DM_REGISTRATION_SUCCESSFUL = 0;
	public static final int C2DM_UNREGISTERED = 1;
	public static final int C2DM_REGISTRATION_ERROR = 2;
	
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	
	public C2DMReceiver() {
		super(Config.C2DM_SENDER);
	}

	@Override
	public void onError(Context context, String errorId) {
        //Toast.makeText(context, "Messaging registration error: " + errorId, Toast.LENGTH_LONG).show();
        Intent c2dmRegisteredIntent = new Intent(C2DM_REGISTERED_STATUS_ACTION);
        c2dmRegisteredIntent.putExtra(INTENT_C2DM_REGISTRATION_STATUS, C2DM_REGISTRATION_ERROR);
        c2dmRegisteredIntent.putExtra(INTENT_C2DM_REGISTRATION_ERROR_ID, errorId);
        sendBroadcast(c2dmRegisteredIntent);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		//String message = intent.getExtras().getString("message");
		String action = (String) intent.getExtras().get("action");
		String id = (String) intent.getExtras().get("id");
        Log.d(TAG, "onMessage, action: " + action + " Id: " + id);
        Config con = new Config(context);
        con.setExpId(Integer.parseInt(id));
        
//		com.msl.intentservice.ExperimentHandler.acquireStaticLock(context);// id is exp id
		
		if(action.equals("ping")){
			Log.d(TAG, "Ping received");
			context.startService(new Intent(context, PongService.class));
		}
		else if(action.equals("experiment") || action.equals("trigexperiment")){
//			Intent exp_intent = new Intent(context, ExperimentHandler.class);
//			exp_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);	
//			context.startService(exp_intent);
			WakefulIntentService1.sendWakefulWork(context, ExperimentHandler.class);
		}
	}

	@Override
	public void onRegistered(Context context, String registrationId)
		throws IOException {
		super.onRegistered(context, registrationId);
        Intent c2dmRegisteredIntent = new Intent(C2DM_REGISTERED_STATUS_ACTION);
        c2dmRegisteredIntent.putExtra(INTENT_C2DM_REGISTRATION_STATUS, C2DM_REGISTRATION_SUCCESSFUL);
        sendBroadcast(c2dmRegisteredIntent);
        Log.v(TAG, "Registration intent sent");
        //auto refresh reg id
        Config con = new Config(context);
        if(con.getMslRegStatus()){
        	NetworkCommunication.refreshRegistrationId(con.getTermId(), C2DMessaging.getRegistrationId(context));
        }
	}

	@Override
	public void onUnregistered(Context context) {
		super.onUnregistered(context);		
        Intent c2dmRegisteredIntent = new Intent(C2DM_REGISTERED_STATUS_ACTION);
        c2dmRegisteredIntent.putExtra(INTENT_C2DM_REGISTRATION_STATUS, C2DM_UNREGISTERED);
        sendBroadcast(c2dmRegisteredIntent);
	}
	

}
