package com.msl.melange;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Build;
import android.util.Log;

public class NetworkCommunication {
	private static final int REGISTRATION_TIMEOUT = 30 * 1000; // ms
	private static final String TOKEN_URI = Config.APP_BASE_URI + ".json";
	private static final String WORK_URI = Config.WKLD_BASE_URI;
	private static final String LOG_TAG = "Push_NetworkComm";

	private static DefaultHttpClient httpClient = null;
	private static JSONObject resp_obj;
	private static String resp_str;

	public static String getworkload(String workloadid) {

		try {
			maybeCreateHttpClient();
			HttpGet get = new HttpGet(WORK_URI + "/" + workloadid
					+ "/workload.json");
			HttpResponse resp = httpClient.execute(get);
			HttpEntity resEntity = resp.getEntity();
			if (resEntity != null) {
				resp_str = EntityUtils.toString(resEntity);
			}
			return resp_str;
			// return resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
		} catch (UnsupportedEncodingException ex) {
			Log.e(LOG_TAG, "UnsupportedEncodingException", ex);
			return null;
		} catch (IOException ex) {
			Log.e(LOG_TAG, "IOException", ex);
			return null;
		}

	}
	
	public static String gettrigworkload(String workloadid) {

		try {
			maybeCreateHttpClient();
			HttpGet get = new HttpGet(WORK_URI + "/" + workloadid
					+ ".json");
			HttpResponse resp = httpClient.execute(get);
			HttpEntity resEntity = resp.getEntity();
			if (resEntity != null) {
				resp_str = EntityUtils.toString(resEntity);
			}
			return resp_str;
			// return resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
		} catch (UnsupportedEncodingException ex) {
			Log.e(LOG_TAG, "UnsupportedEncodingException", ex);
			return null;
		} catch (IOException ex) {
			Log.e(LOG_TAG, "IOException", ex);
			return null;
		}

	}
	
	public static void sendResult(String result, int expid, Context context) {
		File file = new File(context.getFilesDir() , "result");
		
		BufferedOutputStream bs;
		try {
			bs = new BufferedOutputStream(new FileOutputStream(file));
			bs.write(result.toString().getBytes());
			bs.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			maybeCreateHttpClient();
			HttpPost post = new HttpPost(Config.WKLD_BASE_URI + "/" + expid+ "/results");
			FileBody bin = new FileBody(file);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			reqEntity.addPart("log_data", bin);
			post.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(post);
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				Log.i("RESPONSE", EntityUtils.toString(resEntity));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		file.delete();
	}

	public static void sendstatus(String status, String id, Context context) {

		File file = new File(context.getFilesDir() , "hello_file");
		
		BufferedOutputStream bs;
		try {
			bs = new BufferedOutputStream(new FileOutputStream(file));
			bs.write(status.toString().getBytes());
			bs.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			maybeCreateHttpClient();
			HttpPost post = new HttpPost(Config.APP_BASE_URI + "/" + id+ "/statuses");
			FileBody bin = new FileBody(file);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			reqEntity.addPart("log_data", bin);
			post.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(post);
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				Log.i("RESPONSE", EntityUtils.toString(resEntity));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		file.delete();
	}

	public static boolean refreshRegistrationId(String id,
			String c2dmregistrationid) {
		try {
			maybeCreateHttpClient();
			HttpPut put = new HttpPut(Config.APP_BASE_URI + "/" + id);
			ArrayList<BasicNameValuePair> parms = new ArrayList<BasicNameValuePair>();
			parms.add(new BasicNameValuePair("terminal[registration_id]",
					c2dmregistrationid));
			put.setEntity(new UrlEncodedFormEntity(parms));
			HttpResponse resp = httpClient.execute(put);
			HttpEntity resEntity = resp.getEntity();
			if (resEntity != null) {
				Log.i(LOG_TAG, EntityUtils.toString(resEntity));
			}
			return true;
		} catch (IOException ex) {
			Log.e(LOG_TAG, "IOException", ex);
			return false;
		}
	}

	public static String sendRegistrationId(Context context,
			String registrationId) {
		// String accountName = account.name;
		return sendToken(registrationId);
	}

	private static String sendToken(String c2dmregistrationId) {
		try {
			maybeCreateHttpClient();
			HttpPost post = new HttpPost(TOKEN_URI);
			ArrayList<BasicNameValuePair> parms = new ArrayList<BasicNameValuePair>();
			parms.add(new BasicNameValuePair("terminal[name]", Build.MODEL));
			parms.add(new BasicNameValuePair("terminal[description]",
					Build.DEVICE));
			parms.add(new BasicNameValuePair("terminal[os]",
					Build.VERSION.CODENAME));

			parms.add(new BasicNameValuePair("terminal[registration_id]",
					c2dmregistrationId));
			System.out.println(c2dmregistrationId);
			post.setEntity(new UrlEncodedFormEntity(parms));
			HttpResponse resp = httpClient.execute(post);
			HttpEntity resEntity = resp.getEntity();
			if (resEntity != null) {
				// Log.i("RESPONSE",EntityUtils.toString(resEntity));
				try {
					resp_obj = new JSONObject(EntityUtils.toString(resEntity));
					resp_obj = new JSONObject(resp_obj.getString("terminal"));

				} catch (ParseException e) {
					e.printStackTrace();
					return null;
				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}
			}
			try {
				return resp_obj.getString("id");
			} catch (JSONException e) {
				e.printStackTrace();
				return null;
			}
			// return resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
		} catch (UnsupportedEncodingException ex) {
			Log.e(LOG_TAG, "UnsupportedEncodingException", ex);
			return null;
		} catch (IOException ex) {
			Log.e(LOG_TAG, "IOException", ex);
			return null;
		}
	}

	private static void maybeCreateHttpClient() {
		if (httpClient == null) {
			httpClient = new DefaultHttpClient();
			HttpParams params = httpClient.getParams();
			HttpConnectionParams.setConnectionTimeout(params,
					REGISTRATION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, REGISTRATION_TIMEOUT);
			ConnManagerParams.setTimeout(params, REGISTRATION_TIMEOUT);
		}
	}

}
