package com.msl.melange;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.webkit.URLUtil;

import com.dbpreferences.DatabaseBasedSharedPreferences;

public class Config {
	
	private DatabaseBasedSharedPreferences preferences;
	
	public Config (Context context){
		if(preferences==null)
		preferences = new DatabaseBasedSharedPreferences(context);
	}

//   public static final String C2DM_SENDER = "mslkth@gmail.com";
    public static final String C2DM_SENDER = "431983384472";
    public static final String APP_BASE_URI = "http://msl1.it.kth.se/Melange2/terminals";
//   public static final String APP_BASE_URI = "http://130.237.20.241:3000/terminals";
    public static final String WKLD_BASE_URI = "http://msl1.it.kth.se/Melange2/experiments";
//   public static final String WKLD_BASE_URI = "http://130.237.20.241:3000/experiments";
    //Not in use
    public static final String DWNLD_URI = "http://msl1.it.kth.se/Melange2/download?size=";
	public static final String Status_file = "status_file";
	public static String MSL_REG_STATUS = "msl_reg_status";
	public static final String WIFI_STATUS = "wifi_status";
	public static final String EXP_ID_LATEST = "exp_id";
	//Not in use
	public static final String EXP_STR = "exp_str";
	// Current workload execution. If it's the 1st, 2nd, etc time the workload has been run.
	public static final String EXP_CUR_COUNT = "exp_cur_count";
	// Workload count. i.e. how many times the workload will be executed
	public static final String EXP_TOTAL_COUNT = "exp_total_count";
	// How many tasks in total the workload contains
	public static final String TASK_TOTAL_COUNT = "task_total_count";
	// What is the current task that is being performed
	public static final String TASK_CUR_COUNT = "task_cur_count";
	public static final String BAT_STS = "here_goes_the_battery_status";
	public static final String BAT_LVL = "here_goes_the_battery_level";
	public static final String BAT_VOL = "here_goes_the_battery_voltage";
	public static final String Latitude = "here_latitude";
	public static final String Longitude = "here_longitude";
	public static final String Accuracy = "here_location_accuracy";
	public static final String LatitudeNetwork = "here_latitude_network";
	public static final String LongitudeNetwork = "here_longitude_network";
	public static final String AccuracyNetwork = "here_location_accuracy_network";
	
	public static final String SIG_STR = "sig_str";
	public static final String SIG_DBM = "sig_dbm";
	public static final String SIG_ECIO = "sig_ecio";
	public static final String SIG_STRDBM = "sig_strdbm";
	public static final String Cellid = "cell_id";
	public static final String Lac = "lac";
	public static final String WIFI_BSSID = "wifi_bssid";
	
	public static final String MSL_TERM_ID = "msl_termid";
	// It should not be here. It should be in res/values/strings.xml
	public static final String reg_action="Please select an account";
	// It should not be here. It should be in res/values/strings.xml
	public static final String reg_ing="Please Wait While Registering";
	// Not in use
	public static final String BATTERY_VOL = "/sys/class/power_supply/battery/voltage_now";
	// Not in use
	public static final String BATTERY_CAPACITY = "/sys/class/power_supply/battery/capacity";
	public static final String CURRENT_NOW = "/sys/class/power_supply/battery/current_now";
	// Not in use
	public static final String CURRENT_AVG = "/sys/class/power_supply/battery/current_avg";
	static File toFile;
	
	public boolean getMslRegStatus() {
		return preferences.getBoolean(MSL_REG_STATUS, false);
	}

	public void setMslRegStatus(boolean mslRegStatus) {
		preferences.putBoolean(MSL_REG_STATUS, mslRegStatus);
	}

	public String getTermId() {
		return preferences.getString(MSL_TERM_ID, "null");
	}

	public void setTermId(String termid) {
		preferences.putString(MSL_TERM_ID, termid);
	}
	
	public int getSignal() {
		return preferences.getInt(SIG_STR, 0);
	}

	public void setSignal(int signal) {
		preferences.putInt(SIG_STR, signal);
	}
	
	public int getExpId() {
		return preferences.getInt(EXP_ID_LATEST, 0);
	}

	public void setExpId(int expid) {
		preferences.putInt(EXP_ID_LATEST, expid);
	}
	
	public void setLatitudeN(float lat){
		preferences.putFloat(LatitudeNetwork, lat);
	}
	
	public float getLongitudeN(){
		return preferences.getFloat(LongitudeNetwork, 0);
	}
	
	public void setLongitudeN(float longit){
		preferences.putFloat(LongitudeNetwork, longit);
	}
	
	public float getLatitudeN(){
		return preferences.getFloat(LatitudeNetwork, 0);
	}
	
	public void setAccuracyN(float acc){
		preferences.putFloat(AccuracyNetwork, acc);
	}
	
	public float getAccuracyN(){
		return preferences.getFloat(AccuracyNetwork, 0);
	}
	
	public void setLatitude(float lat){
		preferences.putFloat(Latitude, lat);
	}
	
	public float getLongitude(){
		return preferences.getFloat(Longitude, 0);
	}
	
	public void setLongitude(float longit){
		preferences.putFloat(Longitude, longit);
	}
	
	public float getLatitude(){
		return preferences.getFloat(Latitude, 0);
	}
	
	public void setAccuracy(float acc){
		preferences.putFloat(Accuracy, acc);
	}
	
	public float getAccuracy(){
		return preferences.getFloat(Accuracy, 0);
	}
	
	public int getBatPlugged() {
		return preferences.getInt(BAT_STS, 0);
	}

	public void setBatPlugged(int status) {
		preferences.putInt(BAT_STS, status);
	}
	
	public int getBatlvl() {
		return preferences.getInt(BAT_LVL, 0);
	}

	public void setBatlvl(int lvl) {
		preferences.putInt(BAT_LVL, lvl);
	}

	
	public static String getData(String path) throws IOException {
		if (!URLUtil.isNetworkUrl(path)) {
			return path;
		} else {
			
			boolean mExternalStorageAvailable = false;
			boolean mExternalStorageWriteable = false;
			String state = Environment.getExternalStorageState();

			if (Environment.MEDIA_MOUNTED.equals(state)) {
				// We can read and write the media
				mExternalStorageAvailable = mExternalStorageWriteable = true;
			} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
				// We can only read the media
				mExternalStorageAvailable = true;
				mExternalStorageWriteable = false;
			} else {
				// Something else is wrong. It may be one of many other states, but
				// all we need
				// to know is we can neither read nor write
				mExternalStorageAvailable = mExternalStorageWriteable = false;
			}

			if (mExternalStorageAvailable && mExternalStorageWriteable) {
				
				URL url = new URL(path);
				/*URLConnection cn = url.openConnection();
				cn.connect();*/
				
				HttpURLConnection cn = (HttpURLConnection) url.openConnection();
			    cn.setRequestMethod("GET");
			    //cn.setDoOutput(true);
			    cn.connect();
			    
			    InputStream from = cn.getInputStream();
			    Log.v("com.msl.melange","in download");
				if (from == null)
					throw new RuntimeException("stream is null");
				
				toFile = new File(Environment.getExternalStorageDirectory(),"profile");
				
				
				FileOutputStream to = null;
				try {

					to = new FileOutputStream(toFile);
					byte[] buffer = new byte[1024];
					int bytesRead;

					while ((bytesRead = from.read(buffer)) != -1)
						//Log.v("com.msl.melange","downloading");
						to.write(buffer); // write
				} catch (Exception e) {
					e.printStackTrace();
					return "null";
				} finally {
					if (from != null)
						try {
							from.close();
						} catch (Exception e) {
							return "null";
						}
					if (to != null)
						try {
							to.close();
						} catch (Exception e) {
							return "null";
						}
				}
			}
			Log.v("com.msl.melange","download complete");
			return toFile.getAbsolutePath();
		}

	}
	 
}
